module.exports = {
  preset: 'ts-jest',
  moduleDirectories: ['node_modules', 'src'] ,
  moduleFileExtensions: ['js', 'ts'],
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
  setupFiles: ['jest-canvas-mock'],
  setupFilesAfterEnv: ['./src/__test__/setup.ts'],
  moduleNameMapper: {
    '.+\\.(jpg|jpeg|png|gif|webp|svg)$': '<rootDir>/src/__tests__/stub.ts',
  }
};
