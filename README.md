# Pixelwars game

[![TypeScript](https://badgen.net/badge/icon/typescript?icon=typescript&label)](https://www.typescriptlang.org/) [![MIT Licence](https://badgen.net/npm/license/lodash)](https://gitlab.com/318h7/nlink/-/blob/master/LICENSE.md) [![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest)


2D Isometric online multiplayer RPG build with canvas and typescript

## Running the project

To install the required dependencies run

``` shell
yarn install
```

To run the development server

``` shell
yarn dev
```

## QA

Code linter:
``` shell
yarn lint
```

Type check:

``` shell
yarn typecheck
```

## Details

Using webpack with dev server for development and live reload

Assets are packed with the bundle

