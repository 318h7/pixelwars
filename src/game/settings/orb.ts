import { Vector } from 'engine/common';
import { AbilitySettings } from 'engine/core/game/hero/Ability';
import { Animations } from 'engine/types';

export const orbAnimations: Animations = {
  cast: {
    start: 1,
    end: 7,
    speed: 0.07,
    type: 'one-way',
  },
  fly: {
    start: 7,
    end: 10,
    speed: 0.1,
    type: 'round',
  },
  collide: {
    start: 11,
    end: 26,
    speed: 0.06,
    type: 'one-way',
  },
};

export const orbSetting: AbilitySettings = {
  name: 'orb',
  speed: 350,
  correction: new Vector(0, -32),
};
