export const FLOOR_WEIGHTS = [
  { index: 1, weight: 10 },
  { index: 2, weight: 10 },
  { index: 5, weight: 2 },
  { index: 6, weight: 2 },
  { index: 7, weight: 2 },
  { index: 11, weight: 2 },
  { index: 12, weight: 2 },
  { index: 13, weight: 2 },
  { index: 15, weight: 2 },
];

export const TREE_WEIGHTS = [
  { index: 0, weight: 95 },
  { index: 1, weight: 2 },
  { index: 2, weight: 2 },
  { index: 3, weight: 2 },
  { index: 4, weight: 0.5 },
  { index: 14, weight: 0.3 },
  { index: 15, weight: 1 },
  { index: 16, weight: 0.1 },
];

export const OBJECT_WEIGHTS = [
  { index: 0, weight: 90 }, // Place an empty tile most of the tile
  { index: 31, weight: 1 }, // plant
  { index: 32, weight: 1 }, // plant
  { index: 33, weight: 1 }, // plant
  { index: 34, weight: 0.1 }, // logg
  { index: 35, weight: 0.1 }, // logg
  { index: 36, weight: 0.1 }, // grass
  { index: 37, weight: 0.1 }, // grass
  { index: 38, weight: 0.1 }, // grass
  { index: 39, weight: 0.1 }, // deadbush
  { index: 41, weight: 1 }, // rock
  { index: 42, weight: 1 }, // rock
  { index: 43, weight: 1 }, // rock
  // { index: 44, weight: 0.1 }, // big rock
  // { index: 45, weight: 0.1 }, // big rock
  { index: 72, weight: 0.1 }, // puddle
];
