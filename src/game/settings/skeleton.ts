import { SpriteParams, Animations, Directions } from 'engine/types';

const skeletonAnimations: Animations = {
  idle: {
    start: 1,
    end: 4,
    speed: 0.15,
    type: 'ping-pong',
  },
  walk: {
    start: 5,
    end: 12,
    speed: 0.07
  },
  attack: {
    start: 13,
    end: 18,
    speed: 0.11,
    type: 'one-shot',
  },
  call: {
    start: 17,
    end: 20,
    speed: 0.1,
    type: 'one-way',
  },
  block: {
    start: 21,
    end: 22,
    speed: 0.1,
    type: 'one-way',
  },
  die: {
    start: 23,
    end: 28,
    speed: 0.1,
    type: 'one-way',
  },
  shoot: {
    start: 29,
    end: 31,
    speed: 0.1,
    type: 'one-way',
  },
};

const skeletonDirections: Directions = {
  west: 0,
  northWest: 32,
  north: 64,
  northEast: 96,
  east: 128,
  southEast: 160,
  south: 192,
  southWest: 224,
};

export const skeletonParams: SpriteParams = {
  animations: skeletonAnimations,
  directions: skeletonDirections,
  width: 128,
  height: 128,
};
