import { ProjectileDirections } from 'engine/types';

export const arrowDirections: ProjectileDirections = {
  west: 16,
  wff: 1,
  wsf: 2,
  wtf: 3,
  north: 4,
  nff: 5,
  nsf: 6,
  ntf: 7,
  east: 8,
  eff: 9,
  esf: 10,
  etf: 11,
  south: 12,
  sff: 13,
  ssf: 14,
  stf: 15,
};
