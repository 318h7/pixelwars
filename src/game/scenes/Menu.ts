import { ToggleButton, TriggerButton, Vector } from 'engine/common';
import { Game, RenderedObject } from 'engine/core';

export class Menu extends RenderedObject {
  private HEIGHT: number;
  private STROKE: number;
  private dimension: Vector;
  spells: TriggerButton[];
  cameraFollow: ToggleButton;

  constructor(game: Game) {
    super(game);
    this.HEIGHT = 32;
    this.STROKE = 4;
    this.spells = [];

    this.cameraFollow = new ToggleButton(
      game,
      new Vector(this.game.ctx.canvas.width - 24, this.game.ctx.canvas.height - this.HEIGHT + 6),
      on => {
        if (on) {
          this.game.camera.follow(this.game.hero);
        } else {
          this.game.camera.unfollow();
        }
      },
      this.game.loader.getImage('location'),
      !!this.game.camera.following,
      on => {
        if (!on && this.game.camera.isFollowing()) {
          return true;
        }
        if (on && !this.game.camera.isFollowing()) {
          return false;
        }
      }
    );

    this.position = new Vector(
      this.STROKE / 2,
      this.game.ctx.canvas.height - this.HEIGHT - (this.STROKE / 2),
    );
    this.dimension = new Vector(
      this.game.ctx.canvas.width - this.STROKE,
      this.HEIGHT,
    );
  }

  render(): void {
    this.drawBar();
    this.cameraFollow.render();
  }

  update(): void {
    this.cameraFollow.update();
  }

  isOver(): boolean {
    return this.game.mouse.location.inside(this.position, this.position.add(this.dimension));
  }

  private drawBar(): void {
    this.game.ctx.strokeStyle = '#9c9876';
    this.game.ctx.fillStyle = '#d9d6ba';
    this.game.ctx.lineWidth = this.STROKE;
    this.game.ctx.beginPath();
    this.game.ctx.rect(
      this.position.x,
      this.position.y,
      this.dimension.x,
      this.dimension.y,
    );
    this.game.ctx.stroke();
    this.game.ctx.fill();
    this.game.ctx.lineWidth = 1;
  }
}
