import { Game, Layer, GameScene } from 'engine/core';
import { Vector } from 'engine/common';
import { Key } from 'engine/types';

import {
  GRID_H,
  FLOOR_WEIGHTS,
  OBJECT_WEIGHTS,
  TREE_WEIGHTS,
} from 'game/settings';
import { SceneMap } from 'game/map/SceneMap';

import { Menu } from './Menu';

const DIMENSION = 16;

/*
Battle game scene
*/

const STATE = 'battle.state';

export class Battle extends GameScene {
  map: SceneMap;
  menu: Menu;
  private showGrid: boolean;

  constructor(game: Game){
    super(game);
    this.game.setData(STATE, 'idle');

    this.showGrid = false;

    this.map =  new SceneMap(this.game, {
      cols: DIMENSION,
      rows: DIMENSION,
      tsize: GRID_H,
      layers: {
        ground: new Layer('isotiles', DIMENSION, FLOOR_WEIGHTS),
        objects: new Layer('isotiles', DIMENSION, OBJECT_WEIGHTS),
        trees: new Layer(
          'objects',
          DIMENSION,
          TREE_WEIGHTS,
          48,
          112,
          { vertical: 'bottom', horizontal: 'center' }
        ),
      }
    });
  }

  load(): Promise<HTMLImageElement>[] {
    return [
      this.game.loader.loadImage('isotiles', '../assets/iso/iso-64x32-outside.png'),
      this.game.loader.loadImage('objects', '../assets/iso/iso-64x32-objects.png'),
      this.game.loader.loadImage('location', '../assets/location_button.png'),
    ];
  }

  setupMovement(): void {
    this.game.mouse.hookEvent('mouseup', this.moveOnMouse.bind(this));
    this.game.setData(STATE, 'move');
  }

  private moveOnMouse(): void {
    if (!this.map.isSolidTile(this.map.mouseTile) &&
        this.game.helpers.pathFinder.path &&
        this.map.hero.isWithinReach() &&
        this.game.data[STATE] === 'move' &&
        !this.menu.isOver()
    ) {
      this.map.hero.setPath(this.game.helpers.pathFinder.path);
    }
  }

  init(): void {
    this.game.helpers.pathFinder.setGrid(this.map.layers.trees.tiles);

    this.map.setHero(this.game.hero);
    this.game.hero.setMapPosition(2, 2);

    this.game.camera.follow(this.map.hero);

    this.menu = new Menu(this.game);

    this.game.keyboard.registerKeys([
      Key.Left, Key.Right, Key.Up, Key.Down, Key.A,
      Key.B, Key.C, Key.D, Key.F, Key.G, Key.M, Key.S,
    ]);

    this.game.keyboard.hookEvent(Key.Left, () => {
      this.game.camera.follow(null);
      this.game.camera.moveX(-8, true);
    });
    this.game.keyboard.hookEvent(Key.Right, () => {
      this.game.camera.follow(null);
      this.game.camera.moveX(8, true);
    });
    this.game.keyboard.hookEvent(Key.Up, () => {
      this.game.camera.follow(null);
      this.game.camera.moveY(-8, true);
    });
    this.game.keyboard.hookEvent(Key.Down, () => {
      this.game.camera.follow(null);
      this.game.camera.moveY(8, true);
    });

    this.game.keyboard.hookEvent(Key.G, null, () => {
      this.showGrid = !this.showGrid;
    });

    this.game.keyboard.hookEvent(Key.M, null, () => {
      this.game.events.emit('hero.resetAbility');
      this.setupMovement();
    });

    this.game.keyboard.hookEvent(Key.A, null, () => {
      this.game.setData(STATE, 'cast');
      this.game.hero.useAbility('orb');
    });

    this.game.keyboard.hookEvent(Key.S, null, () => {
      this.game.setData(STATE, 'shoot');
    });

    this.game.keyboard.hookEvent(Key.F, null, () => {
      if (this.game.camera.following) {
        this.game.camera.follow(undefined);
      } else {
        this.game.camera.follow(this.map.hero);
      }
    });
  }

  public isOverMenu(): boolean {
    return this.menu.isOver();
  }

  update(delta: number, _time: number): void {
    this.map.update(delta);
    this.menu.update();
  }

  render(): void {
    this.map.drawDepthSortedLayers(['ground', 'objects']);
    if (this.showGrid) {
      this.map.drawGrid();
    }

    const canWalk = this.game.data[STATE] === 'move' && !this.map.hero.isWalking() &&
      this.map.hero.isWithinReach();

    if (this.game.data[STATE] === 'move' && !this.map.hero.isWalking()) {
      this.map.drawWalkableArea();
    }

    if (this.game.data[STATE] === 'cast' &&
      this.map.hero.activeAbility &&
      !this.map.hero.abilityRendering()
    ) {
      this.map.attackTile(this.map.mouseTile);
    }

    if (this.game.helpers.pathFinder.path && canWalk) {
      this.game.helpers.pathFinder.path.forEach(tile => {
        const pathTile = new Vector(tile[0], tile[1]);
        this.map.colorTile(pathTile, '#ffff0060');
      });
    }

    if (canWalk) {
      const color = this.map.isSolidTile(this.map.mouseTile) ? '#ff000060' : '#00ff0060';
      this.map.colorTile(this.map.mouseTile, color);
    }
    this.map.drawDepthSortedLayers(['trees'], true);

    this.menu.render();
    // this.game.helpers.print('camera ' + this.game.camera.location.toString(), 5, 10);
    // this.game.helpers.print('hero   ' + this.map.hero.position.toString(), 5, 20);
  }
}
