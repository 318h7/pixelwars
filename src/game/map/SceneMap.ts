import { Map, MapParams, Hero, Game } from 'engine/core';
import { Vector } from 'engine/common';

export class SceneMap extends Map {
  hero: Hero | undefined;

  constructor(game: Game, params: MapParams) {
    super(game, params);
    this.game.events.once('scene.battle.ready', () => {
      this.game.animations.addAnimation(
        'tiles',
        { start: 2, end: 5, speed: 0.1, type: 'ping-pong' }
      );
    });
  }

  public update(delta: number): void {
    super.update(delta);

    if (this.hero) {
      this.hero.update(delta);
    }

    if (!this.isSolidTile(this.mouseTile) && !this.hero.isWalking()) {
      this.game.helpers.pathFinder.findPath(
        this.vectorToTile(this.hero.position),
        this.mouseTile,
      );
    } else {
      this.game.helpers.pathFinder.reset();
    }
  }

  public setHero(hero: Hero): void {
    this.hero = hero;
  }

  markTile(tile: Vector): void {
    const location = this.tileToVector(tile);
    const color = '#c9ce1490';
    const offset = this.game.animations.getFrame('tiles');
    const length = 10;
    const lMargin = this.tsize - offset;
    const hMargin = this.tsize - length;
    this.game.helpers.line(location.add(offset, offset), location.add(length, offset), color);
    this.game.helpers.line(location.add(offset, offset), location.add(offset, length), color);
    this.game.helpers.line(location.add(lMargin, offset), location.add(lMargin, length), color);
    this.game.helpers.line(location.add(hMargin, offset), location.add(lMargin, offset), color);
    this.game.helpers.line(location.add(lMargin, lMargin), location.add(lMargin, hMargin), color);
    this.game.helpers.line(location.add(hMargin, lMargin), location.add(lMargin, lMargin), color);
    this.game.helpers.line(location.add(offset, hMargin), location.add(offset, lMargin), color);
    this.game.helpers.line(location.add(offset, lMargin), location.add(length, lMargin), color);
  }

  attackTile(tile: Vector): void {
    const location = this.tileToVector(tile);
    const color = '#c9ce1490';
    const offset = this.game.animations.getFrame('tiles');
    const length = 10;
    const lMargin = this.tsize - offset;
    const hMargin = this.tsize - length;
    this.game.ctx.strokeStyle = color;
    this.game.ctx.fillStyle = color;
    this.game.helpers.shape(
      location.add(length, offset),
      location.add(length+offset, length+offset),
      location.add(offset, length),
      location.add(offset, offset),
    );

    this.game.helpers.shape(
      location.add(lMargin, lMargin),
      location.add(lMargin+offset, hMargin+offset),
      location.add(hMargin, hMargin),
      location.add(hMargin, lMargin),
    );
  }

  drawDepthSortedLayers(layers: string[], drawHero?: boolean): void {
    const isLastLayer = (index: number) => index === layers.length - 1;

    for (let c = 0; c < this.cols; c++) {
      for (let r = 0; r < this.rows; r++) {
        layers.forEach((layer, index) => {
          const { mapName, tileWidth, tileHeight, alignment } = this.layers[layer];
          const map = this.game.loader.getImage(mapName);
          const tile = this.getTile(layer, c, r);

          const location = this.tileToVector(new Vector(c, r));

          if (tile !== 0) { // 0 => empty tile
            this.game.helpers.drawTile({ map, tile, location, tileWidth, tileHeight, alignment });
          }
          if (isLastLayer(index) && drawHero) {
            const heroCol = this.getCol(this.hero.position);
            const heroRow = this.getRow(this.hero.position);
            if (heroCol === c && heroRow === r) {
              this.hero.render();
            }
          }
        });
      }
    }
  }

  drawWalkableArea(): void {
    for(let col = 0; col < this.cols; col++) {
      for (let row = 0; row < this.rows; row++) {
        const tile = new Vector(col, row);
        if (this.hero.isWalkableArea(tile)) {
          this.markTile(tile);
        }
      }
    }
  }
}
