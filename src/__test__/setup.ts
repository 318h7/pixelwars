beforeAll(() => {
  const el = document.createElement('canvas');
  el.setAttribute('id', 'test-canvas');

  const body = document.querySelector('body');
  body.appendChild(el);
});
