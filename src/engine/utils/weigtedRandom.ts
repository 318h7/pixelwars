// TODO: move to helpers
export interface WeightIndex {
  weight: number;
  index: number;
}

/*
Return random values with respect to passed weights
*/
export const weightedRandomize = (weightedIndexes: WeightIndex[]) => (): number => {
  let weightTotal = 0;

  for (let i = 0; i < weightedIndexes.length; i++) {
    weightTotal += weightedIndexes[i].weight;
  }

  if (weightTotal <= 0) { return; }

  const rand = Math.random() * weightTotal;
  let sum = 0;
  let randomIndex = -1;

  for (let j = 0; j < weightedIndexes.length; j++) {
    sum += weightedIndexes[j].weight;

    if (rand <= sum) {
      const chosen = weightedIndexes[j].index;

      randomIndex = Array.isArray(chosen)
        ? chosen[Math.floor(Math.random() * chosen.length)]
        : chosen;
      break;
    }
  }

  return randomIndex;
};
