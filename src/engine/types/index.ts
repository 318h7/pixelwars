export type Side =
    'west'
  | 'northWest'
  | 'north'
  | 'northEast'
  | 'east'
  | 'southEast'
  | 'south'
  | 'southWest';

export type ProjectileSide =
    'west'
  | 'wff'
  | 'wsf'
  | 'wtf'
  | 'north'
  | 'nff'
  | 'nsf'
  | 'ntf'
  | 'east'
  | 'eff'
  | 'esf'
  | 'etf'
  | 'south'
  | 'sff'
  | 'ssf'
  | 'stf';

export type HeroState = 'idle' | 'walk' | 'die' | 'attack' | 'shoot' | 'call' | 'block';
export type ProjectileState = 'cast' | 'fly' | 'collide';

export type Directions = Record<Side, number>;
export type ProjectileDirections = Record<ProjectileSide, number>;

export type AnimationType = 'round' | 'ping-pong' | 'one-shot' | 'one-way' | 'reverse';

export interface AnimationParams {
  start: number;
  end: number;
  speed: number;
  type?: AnimationType;
}

export type Animations = Partial<Record<HeroState | ProjectileState, AnimationParams>>;

export enum Key {
  Left = 'ArrowLeft',
  Right = 'ArrowRight',
  Up = 'ArrowUp',
  Down = 'ArrowDown',
  G = 'KeyG',
  S = 'KeyS',
  D = 'KeyD',
  A = 'KeyA',
  C = 'KeyC',
  B = 'KeyB',
  F = 'KeyF',
  M = 'KeyM',
}

export interface SpriteParams {
  animations?: Animations;
  directions?: Directions;
  width?: number;
  height?: number;
}


export type VAlignment = 'top' | 'center' | 'bottom';
export type HAlignment = 'start' | 'center' | 'end';

export interface Alignment {
  vertical: VAlignment,
  horizontal: HAlignment,
}
