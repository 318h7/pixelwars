import EE from 'eventemitter3';

import { Keyboard, Loader, Mouse, Camera } from 'engine/core';
import { Map } from 'engine/core/game/map';
import { AnimationRunner } from '../animations/AnimationRunner';

import { GameHelpers } from '../helpers/GameHelpers';
import { Hero } from '../hero/Hero';
import { GameSettings, GameEngine } from './GameEngine';
import { GameScene } from './GameScene';

/*
Main game ovject holding the context of the game
*/

type Data = Record<string, unknown> | string | number | Array<unknown>;

export class Game extends GameEngine {
  loader: Loader;
  keyboard: Keyboard;
  mouse: Mouse;
  helpers: GameHelpers;
  scene: GameScene;
  map: Map | null;
  camera: Camera;
  hero: Hero;
  heroes: Record<string, Hero>;
  events: EE;
  data: Record<string, Data>;
  public animations: AnimationRunner;
  protected scenes: Record<string, GameScene>;
  private sceneReady: boolean;

  constructor(settings: GameSettings) {
    super(settings);
    this.loader = new Loader();
    this.scenes = {};
    this.heroes = {};
    this.data = {};
    this.keyboard = new Keyboard();
    this.mouse = new Mouse(this.ctx);
    this.helpers = new GameHelpers(this);
    this.camera = new Camera(this);
    this.sceneReady = false;
    this.events = new EE();
    this.animations = new AnimationRunner(this);
  }

  protected render(): void {
    if (this.sceneReady) {
      this.scene.render();
    }
  }

  protected update(delta: number, time: number): void {
    if (this.sceneReady) {
      this.scene.update(delta, time);
    }
    this.camera.update();
    this.animations.update(delta);
  }

  public addScene(name: string, Scene: new (game: Game) => GameScene): void {
    this.scenes[name] = new Scene(this);
  }

  public addHero(name: string, hero: Hero, main?: boolean): void {
    this.heroes[name] = hero;
    if (main) {
      this.hero = hero;
    }
  }

  public setData(name: string, data: Data): void {
    this.data[name] = data;
  }

  public renderScene(name: string): void {
    this.sceneReady = false;
    this.scene = this.scenes[name];
    this.map = this.scene.map;

    Promise.all(this.scene.load()).then(() => {
      this.scene.init();
      this.sceneReady = true;
      this.events.emit(`scene.${name}.ready`);
    });
  }
}
