import { GameScene } from './GameScene';

export abstract class SceneObject {
  scene: GameScene;

  constructor(scene: GameScene) {
    this.scene = scene;
  }
}
