import { Vector } from '../../../common/Vector';
import { Game } from './Game';

/*
Abstract game elements
*/
export abstract class GameObject {
  protected game: Game;

  constructor(game: Game) {
    this.game = game;
  }
}

export abstract class RenderedObject extends GameObject {
  position: Vector;

  abstract update(delta: number, time: number): void;
  abstract render(): void;
}
