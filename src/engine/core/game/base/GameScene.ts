import { Menu } from 'game/scenes/Menu';

import { Game } from './Game';
import { Map} from '../map/Map';
import { RenderedObject } from './GameObject';

/*
Game scene base class
*/
export abstract class GameScene extends RenderedObject {
  abstract load(): Promise<HTMLImageElement>[];
  abstract init(): void;
  abstract map: undefined | Map;
  // TODO: rework to panels
  abstract menu: Menu;
  abstract isOverMenu(): boolean;

  constructor(game: Game) {
    super(game);
  }
}
