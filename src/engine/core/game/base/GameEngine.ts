export interface GameSettings {
  width: number;
  height: number;
  rootId: string;
}

/*
Canvas game enine running main loop
*/
export abstract class GameEngine {
  private previousElapsed: number;
  protected settings: GameSettings;
  public ctx: CanvasRenderingContext2D;

  protected abstract render(): void;
  protected abstract update(delta: number, time: number): void;

  public constructor(settings: GameSettings) {
    this.settings = settings;
    this.previousElapsed = 0;
    const context = <CanvasRenderingContext2D>(
      document.getElementById(settings.rootId) as HTMLCanvasElement
    ).getContext('2d');
    this.ctx = context;
    this.ctx.canvas.width = this.settings.width;
    this.ctx.canvas.height = this.settings.height;

    window.requestAnimationFrame(this.tick.bind(this));
  }

  protected tick(elapsed: number): void {
    window.requestAnimationFrame(this.tick.bind(this));
    // clear previous frame
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

    // compute delta time in seconds
    const delta = (elapsed - this.previousElapsed) / 1000.0;
    this.previousElapsed = elapsed;

    this.render();
    this.update(delta, elapsed);
  }
}
