import { GRID_H, GRID_W } from 'game/settings';
import { Game } from 'engine/core';
import { Vector } from 'engine/common';

import { Animations, SpriteParams } from 'engine/types';

import { AnimatedHero } from './HeroObject';
import { Ability, AbilitySettings } from './Ability';

// TODO: extract base hero
// TODO: extract speed calculations to mover class (MovingObject?)
export class Hero extends AnimatedHero {
  private name: string;
  // pixels per second
  private maxSpeed: number;
  private locationCorrection: Vector;
  private destination: Vector | undefined;
  private path: number[][] | undefined;
  private limit: number;
  private correction: Vector;
  private velocity: Vector;
  private abilities: Record<string, Ability>;
  activeAbility: string | undefined;
  width: number;
  height: number;
  sprite: HTMLImageElement;

  constructor(
    game: Game,
    sprite: HTMLImageElement,
    {
      animations,
      directions,
      width = GRID_W,
      height = GRID_H,
    }: SpriteParams,
    coordinates?: Vector,
    correction?: Vector,
  ) {
    super(game, animations, directions);
    this.position = new Vector(0, 0);
    this.correction = correction;
    this.width = width;
    this.height = height;
    this.sprite = sprite;
    this.abilities = {};
    this.velocity = new Vector(0, 0);
    this.limit = 5;

    this.maxSpeed = 128;
  }

  render(): void {
    this.game.helpers.drawTile({
      map: this.sprite,
      tile: this.getTile(),
      location: this.position,
      tileWidth: this.width,
      tileHeight: this.height,
      alignment: { vertical: 'bottom', horizontal: 'center' },
      manualCorrection: this.correction,
    });

    this.abilities[this.activeAbility]?.render();
  }

  update(delta: number): void {
    this.followPath();
    this.followDestination();

    this.abilities[this.activeAbility]?.update(delta);

    // move hero
    if (!this.velocity.isZero()) {
      // TODO: calculate correct movement speed
      // diagonal movement is not equal in percieved speed
      const speed = this.velocity.x !== 0 && this.velocity.y !== 0
        ? this.maxSpeed / 2
        : this.maxSpeed;

      this.position = this.position.add(
        this.velocity.mult(speed * delta).round()
      );
    }
  }

  public setMapPosition(col: number, row: number): void {
    this.position = new Vector(
      this.game.map.getX(col),
      this.game.map.getY(row),
    );
  }

  public useAbility(name: string): void {
    this.activeAbility = name;
    this.abilities[name]?.use();
    this.game.events.once('hero.resetAbility', () => {
      this.activeAbility = undefined;
    });
  }

  public abilityRendering(): boolean {
    return this.abilities[this.activeAbility]?.hasState() ?? false;
  }

  public addAbility(params: AbilitySettings, animations: Animations): void {
    this.abilities[params.name] = new Ability(this, this.game, params, animations);
  }

  isWithinReach(): boolean {
    return this.game.helpers.pathFinder.path?.length
      ? this.game.helpers.pathFinder.path.length - 1 <= this.limit
      : true;
  }

  isWalkableArea(tile: Vector): boolean {
    const heroTile = this.game.map.vectorToTile(this.position);
    return Math.abs(heroTile.x - tile.x) <= this.limit &&
      Math.abs(heroTile.y - tile.y) <= this.limit;
  }

  public setPath(path: number[][]): void {
    if (path.length > 1) {
      this.path = path;
    }
  }

  private move(destination: Vector): void {
    this.destination = new Vector(
      this.game.map.getX(destination.x),
      this.game.map.getY(destination.y),
    );
    this.setState('walk');
  }

  public setVelocity(vecotor: Vector): void {
    this.velocity = vecotor;
    this.updateDirection(this.velocity);
  }

  public isWalking(): boolean {
    return !this.velocity.isZero() || this.path !== undefined;
  }

  private hasMomentum(): boolean {
    return !this.velocity.isZero();
  }

  private followPath(): void {
    if (this.path && this.path.length && !this.hasMomentum()) {
      const nextVector = this.path.shift();
      this.move(new Vector(nextVector[0], nextVector[1]));
    } else if (this.path && !this.hasMomentum()) {
      this.path = undefined;
      this.setState('idle');
    }
  }

  private followDestination(): void {
    if (this.destination) {
      this.setVelocity(
        this.position.sub(this.destination).negate().clamp(-1, 1)
      );

      if (this.position.isClose(this.destination)) {
        this.position = this.destination;
        this.destination = undefined;
        this.velocity.reset();
      }
    }
  }
}
