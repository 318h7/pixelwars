import { Vector } from 'engine/common/Vector';
import { Animations } from 'engine/types';

import { Game } from '../base/Game';
import { Hero } from './Hero';
import { HeroObject } from './HeroObject';
import { Projectile } from './Projectile';

export interface AbilitySettings {
  name: string;
  speed: number;
  correction?: Vector;
}

export class Ability extends HeroObject {
  name: string;
  projectile: Projectile;

  constructor(
    hero: Hero,
    game: Game,
    { name, speed, correction }: AbilitySettings,
    animations: Animations,
  ) {
    super(hero, game);
    this.name = name;

    this.projectile = new Projectile(
      this.game,
      this.game.loader.getImage(name),
      speed,
      animations,
      correction,
    );
  }

  update(delta: number): void {
    if (!this.projectile.position.isZero()) {
      if (this.projectile.isStateFinished('cast')) {
        this.hero.setState('idle');
      }

      this.projectile.update(delta);
    }
  }

  render(): void {
    this.projectile.render();
  }

  use(): void {
    this.setup();
  }

  setup(): void {
    this.game.mouse.hookEvent('mouseup', () => {
      if (!this.game.scene.isOverMenu()) {
        this.launch(this.game.map.mouseTile);
        this.hero.setState('shoot');
      } else {
        this.setup();
      }
    }, true);
  }

  launch(destination: Vector): void {
    const dir = this.game.map.tileToVector(destination)
      .sub(this.hero.position)
      .normalize().round();
    this.hero.updateDirection(dir);
    this.projectile.setMotion(this.hero.position, destination);
  }

  hasState(): boolean {
    return !!this.projectile.state;
  }
}
