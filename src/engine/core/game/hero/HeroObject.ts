import { Vector } from 'engine/common';
import { Animations, Directions, HeroState, ProjectileState } from 'engine/types';

import { Game } from '../base/Game';
import { RenderedObject, GameObject } from '../base/GameObject';
import { SpriteDirections } from '../SpriteDirections';
import { Hero } from './Hero';

export abstract class HeroObject extends GameObject {
  hero: Hero;

  constructor(hero: Hero, game: Game) {
    super(game);
    this.hero = hero;
  }
}

export abstract class RenderedHeroObject extends RenderedObject {
  hero: Hero;

  constructor(hero: Hero, game: Game) {
    super(game);
    this.hero = hero;
  }
}

export abstract class AnimatedHero extends RenderedObject {
  protected animations: Animations;
  protected direction: SpriteDirections;
  protected state: HeroState;

  constructor(game: Game, animations: Animations, directions?: Directions){
    super(game);
    this.state = 'idle';
    this.animations = animations;
    this.game.animations.addAnimation('hero', this.animations[this.state]);
    this.direction = new SpriteDirections(directions);
  }

  public setState(state: HeroState): void {
    if (this.state !== state) {
      this.state = state;
      this.game.animations.addAnimation('hero', this.animations[this.state]);
    }
  }

  public updateDirection(vector: Vector): void {
    this.direction.updateDirection(vector);
  }

  public getTile(): number {
    return this.direction.getTile(this.game.animations.getFrame('hero'));
  }
}

export abstract class AnimatedProjectile extends RenderedObject {
  protected animations: Animations;
  state: ProjectileState | undefined;

  constructor(game: Game, animations: Animations){
    super(game);
    this.animations = animations;
  }

  public setState(state: ProjectileState): void {
    if (this.state !== state) {
      this.state = state;
      this.game.animations.addAnimation('projectile', this.animations[this.state]);
    }
  }

  public getTile(): number {
    return this.game.animations.getFrame('projectile');
  }
}
