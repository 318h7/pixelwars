import { Vector } from 'engine/common';
import { Animations } from 'engine/types';

import { Game } from '../base/Game';
import { AnimatedProjectile } from './HeroObject';

// TODO: rework into abstract class
// TODO: extract abstract Mover class
// FIX: jitter animation -_-
export class Projectile extends AnimatedProjectile {
  private speed: number;
  private destination: Vector;
  private velocity: Vector;
  private acceleration: Vector;
  private sprite: HTMLImageElement;
  private correction: Vector;
  private killPoint: Vector;

  constructor(
    game: Game,
    sprite: HTMLImageElement,
    speed: number,
    animations: Animations,
    correction = new Vector(0, 0),
  ) {
    super(game, animations);
    this.speed = speed;
    this.sprite = sprite;
    this.velocity = new Vector(0, 0);
    this.position = new Vector(0, 0);
    this.acceleration = new Vector(0, 0);
    this.destination = new Vector(0, 0);
    this.correction = correction;
    this.killPoint = new Vector(0, 0);
  }

  setMotion(location: Vector, destination: Vector): void {
    this.setState('cast');

    const destTile = this.game.map.tileToVector(destination);
    const destTileCenter = this.game.camera.tileCorrection(
      destTile
    );

    this.position = location;
    this.destination = destTileCenter;
  }

  update(delta: number): void {
    if (!this.position.isZero()) {
      if (this.state === 'fly') {
        this.fly(delta);
      }

      if (this.isStateFinished('cast')) {
        this.setState('fly');
      }
      if (this.destination.isClose(this.position.add(this.killPoint), 5) || this.isCollided()) {
        this.setState('collide');
      }
    }
    if (this.isOutsideMap() || this.isStateFinished('collide')) {
      this.reset();
    }
  }

  render(): void {
    if (!this.position.isZero()) {
      this.game.helpers.drawTile({
        map: this.sprite,
        tile: this.getTile(),
        location: this.position,
        manualCorrection: this.correction,
      });
    }
  }

  public isStateFinished(state: string): boolean {
    return this.state === state && this.game.animations.hasFinished('projectile');
  }

  private isCollided(): boolean {
    const tile = this.game.map.vectorToTile(this.position);
    return this.game.map.isSolidTile(tile);
  }

  private isOutsideMap(): boolean {
    const corrected = this.game.helpers.converter.cartToIso(
      this.game.camera.isoCorrection(this.position)
    );
    return corrected.x < 0 || corrected.x  > this.game.ctx.canvas.width &&
      corrected.y < 0 || corrected.y  > this.game.ctx.canvas.height;
  }

  private fly(delta: number): void {
    this.acceleration = this.destination
      .sub(this.position.add(this.killPoint))
      .normalize()
      .mult(this.speed * delta);

    // this.velocity = this.velocity
    //   .add(this.acceleration)
    //   .limit(this.speed)
    //   .round();

    this.position = this.position.add(this.acceleration).round();
  }

  private reset(): void {
    this.velocity.reset();
    this.position.reset();
    this.acceleration.reset();
    this.destination.reset();
    this.game.events.emit('hero.resetAbility');
    this.state = undefined;
  }
}
