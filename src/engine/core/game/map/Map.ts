import { Layer } from 'engine/core';
import { Vector } from 'engine/common';

import { Game } from '../base/Game';
import { GameObject } from '../base/GameObject';

export interface MapParams {
  cols: number;
  rows: number;
  tsize: number;
  layers: Record<string, Layer>;
}

/*
Generic Game Map
*/
export class Map extends GameObject {
  cols: number;
  rows: number;
  tsize: number;
  layers: Record<string, Layer>;
  mouseTile: Vector;
  dimensions: Vector;

  constructor(game: Game, { cols, rows, tsize, layers }: MapParams) {
    super(game);
    this.cols = cols;
    this.rows = rows;
    this.tsize = tsize;
    this.layers = layers;

    this.mouseTile = new Vector(0, 0);
    this.dimensions = new Vector(this.cols * this.tsize, this.rows * this.tsize);
  }

  public update(_delta: number): void {
    this.updateMouseTile();
  }

  colorTile(tile: Vector, color: string): void {
    const location = this.tileToVector(tile);
    this.game.helpers.rect(location, this.tsize, this.tsize, color);
  }

  updateMouseTile(): void {
    const isoMouse = this.game.camera.cartCorrection(
      this.game.helpers.converter.isoToCart(this.game.mouse.location)
    );
    this.mouseTile = this.vectorToTile(isoMouse);
  }

  drawGrid(): void {
    for (let r = 0; r <= this.rows; r++) {
      const location = new Vector(0, r * this.tsize);

      this.game.helpers.line(
        location,
        location.setX(this.dimensions.x),
        '#00ff00',
      );
    }
    for (let c = 0; c <= this.cols; c++) {
      const location = new Vector(c * this.tsize, 0);

      this.game.helpers.line(
        location,
        location.setY(this.dimensions.y),
        '#00ff00',
      );
    }
  }

  getTile(layer: string, col: number, row: number): number {
    return this.layers[layer].tiles?.[row]?.[col] ?? 0;
  }

  vectorToTile(vector: Vector): Vector {
    return new Vector(
      this.getCol(vector),
      this.getRow(vector),
    );
  }

  tileToVector(tile: Vector): Vector {
    return new Vector(
      this.getX(tile.x),
      this.getY(tile.y),
    );
  }

  isSolidTile(location: Vector): boolean {
    return this.layers.trees.tiles[location.y]?.[location.x] !== 0;
  }

  getCol({ x }: Partial<Vector>): number {
    return Math.floor(x / this.tsize);
  }

  getRow({ y }: Partial<Vector>): number {
    return Math.floor(y / this.tsize);
  }

  getX(col: number): number {
    return col * this.tsize;
  }

  getY(row: number): number {
    return row * this.tsize;
  }
}
