import { GRID_W, GRID_H } from 'game/settings';
import { Alignment } from 'engine/types';
import { weightedRandomize, WeightIndex } from 'engine/utils';

export class Layer {
  mapName: string;
  filler: () => number;
  dimension: number;
  tileWidth?: number;
  tileHeight?: number;
  tiles: number[][];
  alignment?: Alignment;

  constructor(
    mapName: string,
    dimension: number,
    weights: WeightIndex[],
    width = GRID_W,
    height = GRID_H,
    alignment?: Alignment,
  ) {
    this.tileWidth = width;
    this.tileHeight = height;
    this.mapName = mapName;
    this.dimension = dimension;
    this.filler = weightedRandomize(weights);
    this.tiles = this.fillMap();
    this.alignment = alignment;
  }

  private fillMap() {
    return Array(this.dimension).fill(null).map(
      () => Array(this.dimension).fill(null).map(() => this.filler()),
    );
  }
}
