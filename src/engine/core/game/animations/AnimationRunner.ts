import { AnimationParams } from 'engine/types';
import { Game, GameObject } from '../base';

import { Animation } from './Animation';

export class AnimationRunner extends GameObject {
  private animations: Record<string, Animation>;

  constructor(game: Game) {
    super(game);
    this.animations = {};
  }

  public addAnimation(name: string, animation: AnimationParams): void {
    this.animations[name] = new Animation(animation);
  }

  public removeAnimation(name: string): void {
    delete this.animations[name];
  }

  public update(delta: number): void {
    Object.keys(this.animations).forEach(name => {
      if (!this.animations[name]?.hasFinished) {
        this.animations[name].update(delta);

        if (this.animations[name]?.hasFinished) {
          this.game.events.emit(`animation.${name}.finished`);
        }
      }
    });
  }

  public getFrame(name: string): number {
    return this.animations[name]?.frame;
  }

  public hasFinished(name: string): boolean {
    return this.animations[name]?.hasFinished;
  }
}
