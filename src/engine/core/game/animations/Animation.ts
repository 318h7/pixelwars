import { AnimationType, AnimationParams } from 'engine/types';

interface AnimationSettings extends AnimationParams {
  step?: number;
}

export class Animation {
  public hasFinished: boolean;
  public frame: number;
  public type: AnimationType;
  protected speed: number;
  protected start: number;
  protected end: number;
  protected step: number;
  private timeElapsed: number;

  constructor({
    speed,
    start,
    end,
    step = 1,
    type = 'round',
  }: AnimationSettings) {
    this.speed = speed;
    this.start = start;
    this.end = end;
    this.type = type;
    this.timeElapsed = 0;
    this.hasFinished = false;
    this.frame = this.start;
    this.step = step;
  }

  public update(delta: number): void {
    this.timeElapsed += delta;

    if (this.timeElapsed >= this.speed && !this.hasFinished) {
      this.frame += this.step;
      this.timeElapsed = 0;
    }

    if (this.frame === this.end && (this.type === 'one-shot' || this.type === 'one-way')) {
      this.hasFinished = true;
    }
    if (this.frame > this.end && (this.type === 'one-shot' || this.type === 'one-way')) {
      this.frame = this.end;
    }
    if (this.frame > this.end && this.type === 'round') {
      this.frame = this.start;
    }

    if (this.frame > this.end && this.type === 'ping-pong') {
      this.step = -1;
      this.frame -= 2;
    }

    if (this.frame === this.start && this.type === 'ping-pong') {
      this.step = 1;
    }
  }
}
