import { Hero, RenderedObject } from 'engine/core';
import { Vector } from 'engine/common';

import { GRID_H } from 'game/settings';

import { Game } from './base/Game';
import { GameObject } from './base/GameObject';

const HALF_TILE = GRID_H / 2;

export class Camera extends GameObject {
  location: Vector;
  width: number;
  height: number;
  following: RenderedObject | null;
  // TODO: migrate to separate VelocityCounter
  private acceleration: number;
  private velocity: Vector;
  private maxSpeed: number;
  private center: Vector;

  constructor(game: Game) {
    super(game);
    this.location = new Vector(0, 0);
    this.width = this.game.ctx.canvas.width;
    this.height = this.game.ctx.canvas.height;
    this.following = null;
    this.center = this.game.helpers.converter.isoToCart(
      new Vector((this.width / 2) - GRID_H, this.height / 2)
    );
    this.acceleration = 0;
    this.velocity = new Vector(0, 0);
    this.maxSpeed = 64;
  }

  public follow(sprite: Hero): void {
    this.following = sprite;
  }

  public unfollow(): void {
    this.following = null;
  }

  public isFollowing(): boolean {
    return !!this.following;
  }

  // isometric
  public isoMoveX(delta: number): void {
    this.location = this.location.add(delta, -delta);
  }

  // isometric
  public isoMoveY(delta: number): void {
    this.location = this.location.add(delta, delta);
  }

  public moveX(delta: number, accelerate?: boolean): void {
    this.velocity = new Vector(delta/2, 0);
    if (accelerate) {
      this.acceleration = 1.0;
    }
  }

  public moveY(delta: number, accelerate?: boolean): void {
    this.velocity = new Vector(0, delta);
    if (accelerate) {
      this.acceleration = 1.0;
    }
  }

  public update(): void {
    if (this.acceleration > 0 && !this.following) {
      this.acceleration -= 0.05;
      if (Math.abs(this.velocity.x)) {
        this.isoMoveX(Math.round(this.velocity.x * this.acceleration));
      }
      if (Math.abs(this.velocity.y)) {
        this.isoMoveY(Math.round(this.velocity.y * this.acceleration));
      }
    } else {
      this.velocity.reset();
      this.acceleration = 0;
    }
    // assume followed sprite should be placed at the center of the screen
    // whenever possible
    if (this.following) {
      // make the camera follow the sprite
      this.location = this.following.position.sub(this.center);
    }
  }

  public cameraCorrection(point: Vector): Vector {
    return point.sub(this.location);
  }

  public isoCorrection(point: Vector): Vector {
    return point.sub(this.location).add(HALF_TILE, -HALF_TILE);
  }

  public cartCorrection(point: Vector): Vector {
    return point.add(this.location).add(-HALF_TILE, HALF_TILE);
  }

  public tileCorrection(point: Vector): Vector {
    return point.add(HALF_TILE, HALF_TILE);
  }
}
