import { Directions } from 'engine/types';
import { Vector } from 'engine/common';

export class SpriteDirections {
  private offset: number;
  private directions: Directions;

  constructor(directions?: Directions) {
    this.directions = directions;
    this.offset = directions?.south ?? 0;
  }

  public getTile(frame: number): number {
    return this.offset + frame;
  }

  public updateDirection(velocity: Vector): void {
    if (!this.directions) {
      return;
    }

    const { x, y } = velocity;
    if (x === 0 && y === 0) {
      return;
    }
    if (x === -1 && y === 1) {
      this.offset = this.directions['west'];
      return;
    }
    if (x === -1 && y === 0) {
      this.offset = this.directions['northWest'];
      return;
    }
    if (x === -1 && y === -1) {
      this.offset = this.directions['north'];
      return;
    }
    if (x === 0 && y === -1) {
      this.offset = this.directions['northEast'];
      return;
    }
    if (x === 1 && y === -1) {
      this.offset = this.directions['east'];
      return;
    }
    if (x === 1 && y === 0) {
      this.offset = this.directions['southEast'];
      return;
    }
    if (x === 1 && y === 1) {
      this.offset = this.directions['south'];
      return;
    }
    if (x === 0 && y === 1) {
      this.offset = this.directions['southWest'];
      return;
    }
  }
}
