import { AStarFinder } from 'astar-typescript';

import { Vector } from '../../../common/Vector';

export class Pathfinder {
  public path: number[][] | null;
  private finder: AStarFinder;
  private pathFrom: Vector | null;
  private pathTo: Vector | null;

  public setGrid(matrix: number[][]): void {
    this.finder = new AStarFinder({
      grid: { matrix },
      includeStartNode: true,
      includeEndNode: true
    });
  }

  public reset(): void {
    this.pathTo = null;
    this.path = null;
  }

  private mapGrid(grid: number[][]): number[][] {
    return grid.map(column => column.map(item => item > 0 ? 1 : 0));
  }

  private shouldCalculate(to: Vector): boolean {
    return !to.isEqual(this.pathTo);
  }

  public findPath(pathFrom: Vector, pathTo: Vector): void {
    if (this.shouldCalculate(pathTo)) {
      this.pathFrom = pathFrom;
      this.pathTo = pathTo;
      this.path = this.finder.findPath(pathFrom, pathTo);
    }
  }
}
