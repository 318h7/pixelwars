import { Vector } from 'engine/common';
import { Game } from 'engine/core';
import { DrawTile } from './DrawTile';

const getImage = (width: number, height: number) => {
  const img = new Image();
  img.width = width;
  img.height = height;

  return img;
};

describe('DrawTile', () => {
  let game: Game = null;
  let drawer: DrawTile = null;

  beforeAll(() => {
    game = new Game({ width: 640, height: 360, rootId: 'test-canvas' });
    drawer = new DrawTile(game);
  });

  afterEach(() => {
    (game?.ctx?.drawImage as jest.Mock)?.mockClear();
  });

  it('skips early on 0 tile', async () => {
    const img = getImage(400, 200);

    drawer.drawTile({
      map: img,
      tile: 0,
      location: new Vector(20, 20),
    });

    expect(game.ctx.drawImage).not.toHaveBeenCalledWith();
  });

  it('draws nothing when exceeding box', async () => {
    const img = getImage(128, 64);

    drawer.drawTile({
      map: img,
      tile: 5,
      location: new Vector(20, 20),
      tileWidth: 64,
      tileHeight: 32,
    });

    expect(game.ctx.drawImage).not.toHaveBeenCalledWith();
  });

  it('draws nothing when exceeding strip', async () => {
    const img = getImage(128, 32);

    drawer.drawTile({
      map: img,
      tile: 3,
      location: new Vector(20, 20),
      tileWidth: 64,
      tileHeight: 32,
    });

    expect(game.ctx.drawImage).not.toHaveBeenCalledWith();
  });

  it('gets correct start position', async () => {
    const img = getImage(400, 200);

    drawer.drawTile({
      map: img,
      tile: 1,
      location: new Vector(20, 20),
      tileWidth: 64,
      tileHeight: 32,
      isometric: false,
      camCorrection: false,
    });

    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, 20, 20, 64, 32);
  });

  it('draws a horisontal strip layout', async () => {
    const img = getImage(400, 200);
    const tileParams = {
      map: img,
      tile: 1,
      location: new Vector(20, 20),
      tileWidth: 100,
      tileHeight: 200,
      isometric: false,
      camCorrection: false,
    };

    drawer.drawTile(tileParams);
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 0, 100, 200, 20, 20, 100, 200);

    drawer.drawTile({ ...tileParams, tile: 2});
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 100, 0, 100, 200, 20, 20, 100, 200);

    drawer.drawTile({ ...tileParams, tile: 3 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 200, 0, 100, 200, 20, 20, 100, 200);

    drawer.drawTile({ ...tileParams, tile: 4 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 300, 0, 100, 200, 20, 20, 100, 200);
  });

  it('draws a vertical strip layout', async () => {
    const img = getImage(200, 400);
    const tileParams = {
      map: img,
      tile: 1,
      location: new Vector(20, 20),
      tileWidth: 200,
      tileHeight: 100,
      isometric: false,
      camCorrection: false,
    };

    drawer.drawTile(tileParams);
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 0, 200, 100, 20, 20, 200, 100);

    drawer.drawTile({ ...tileParams, tile: 2});
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 100, 200, 100, 20, 20, 200, 100);

    drawer.drawTile({ ...tileParams, tile: 3 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 200, 200, 100, 20, 20, 200, 100);

    drawer.drawTile({ ...tileParams, tile: 4 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 300, 200, 100, 20, 20, 200, 100);
  });

  it('draws a box layout', async () => {
    const img = getImage(400, 200);
    const tileParams = {
      map: img,
      tile: 1,
      location: new Vector(20, 20),
      tileWidth: 100,
      tileHeight: 100,
      isometric: false,
      camCorrection: false,
    };

    drawer.drawTile(tileParams);
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 2});
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 100, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 3 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 200, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 4 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 300, 0, 100, 100, 20, 20, 100, 100);

    // layer 2
    drawer.drawTile({ ...tileParams, tile: 5 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 100, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 6 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 100, 100, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 7 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 200, 100, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 8 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 300, 100, 100, 100, 20, 20, 100, 100);
  });

  it('draws a box with excess width', async () => {
    const img = getImage(420, 200);
    const tileParams = {
      map: img,
      tile: 1,
      location: new Vector(20, 20),
      tileWidth: 100,
      tileHeight: 100,
      isometric: false,
      camCorrection: false,
    };

    drawer.drawTile(tileParams);
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 2});
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 100, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 3 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 200, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 4 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 300, 0, 100, 100, 20, 20, 100, 100);

    drawer.drawTile({ ...tileParams, tile: 5 });
    expect(game.ctx.drawImage).toHaveBeenLastCalledWith(img, 0, 100, 100, 100, 20, 20, 100, 100);
  });

  it('gets correct one tile sprite', async () => {
    const img = getImage(64, 32);

    drawer.drawTile({
      map: img,
      tile: 1,
      location: new Vector(20, 20),
      tileWidth: 64,
      tileHeight: 32,
      isometric: false,
      camCorrection: false,
    });

    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, 20, 20, 64, 32);
  });

  it('applies iso corection', async () => {
    const img = getImage(64, 32);
    const location = new Vector(20, 20);

    drawer.drawTile({
      map: img,
      tile: 1,
      location,
      tileWidth: 64,
      tileHeight: 32,
      isometric: true,
      camCorrection: false,
    });
    const isoPos = game.helpers.converter.cartToIso(location);
    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, isoPos.x, isoPos.y, 64, 32);
  });

  it('applies camera corection', async () => {
    const img = getImage(64, 32);
    const location = new Vector(20, 20);

    drawer.drawTile({
      map: img,
      tile: 1,
      location,
      tileWidth: 64,
      tileHeight: 32,
      isometric: false,
      camCorrection: true,
    });
    const camPos = game.camera.cameraCorrection(location);
    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, camPos.x, camPos.y, 64, 32);
  });

  it('applies manual corection', async () => {
    const img = getImage(64, 32);
    const location = new Vector(20, 20);

    drawer.drawTile({
      map: img,
      tile: 1,
      location,
      tileWidth: 64,
      tileHeight: 32,
      isometric: false,
      camCorrection: false,
      manualCorrection: new Vector(-20, -20),
    });
    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, 0, 0, 64, 32);
  });

  it('scales the tile', async () => {
    const img = getImage(64, 32);
    const location = new Vector(20, 20);

    drawer.drawTile({
      map: img,
      tile: 1,
      location,
      tileWidth: 64,
      tileHeight: 32,
      isometric: false,
      camCorrection: false,
      scale: new Vector(200, 200),
    });
    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, 20, 20, 200, 200);
  });

  it('applies manual correction after iso corection', async () => {
    const img = getImage(64, 32);
    const location = new Vector(20, 20);
    const manualCorrection = new Vector(-20, -20);

    drawer.drawTile({
      map: img,
      tile: 1,
      location,
      tileWidth: 64,
      tileHeight: 32,
      isometric: true,
      camCorrection: false,
      manualCorrection,
    });
    const isoPos = game.helpers.converter.cartToIso(location).add(manualCorrection);
    expect(game.ctx.drawImage).toHaveBeenCalledWith(img, 0, 0, 64, 32, isoPos.x, isoPos.y, 64, 32);
  });
});
