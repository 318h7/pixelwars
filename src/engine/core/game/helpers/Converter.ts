import { Vector } from 'engine/common/Vector';

export class Converter {
  private offset: Vector;

  constructor(offsetX: number, offsetY: number) {
    this.offset = new Vector(offsetX, offsetY);
  }

  cartToIso({ x: carX, y: carY }: Vector): Vector {
    return new Vector(
      Math.round(carX - carY + this.offset.x),
      Math.round(((carY + carX) / 2.0) + this.offset.y),
    );
  }

  isoToCart({ x: isoX, y: isoY }: Vector): Vector {
    const adjX = isoX - this.offset.x;
    const adjY = isoY - this.offset.y;

    return new Vector(
      ((2 * adjY) + adjX) / 2,
      ((2 * adjY) - adjX) / 2,
    );
  }
}
