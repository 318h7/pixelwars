import { GRID_H, GRID_W } from 'game/settings';
import { Alignment } from 'engine/types';
import { Vector } from 'engine/common/Vector';

import { Game } from '../base/Game';

export interface TileParams {
  map: HTMLImageElement,
  tile: number,
  location: Vector,
  tileWidth?: number,
  tileHeight?: number,
  scale?: Vector,
  debug?: boolean,
  alignment?: Alignment,
  manualCorrection?: Vector,
  isometric?: boolean,
  camCorrection?: boolean,
}

export class DrawTile {
  private game: Game;

  public constructor(game: Game) {
    this.game = game;
  }

  public drawTile({
    map,
    tile,
    location,
    tileWidth = GRID_W,
    tileHeight = GRID_H,
    scale = new Vector(tileWidth, tileHeight),
    manualCorrection,
    alignment,
    isometric = true,
    camCorrection = true,
    debug,
  }: TileParams): void {
    if (tile === 0) return;

    const { ctx } = this.game;
    const tilesPerRow = Math.floor(map.width / tileWidth);
    const tilesPerColum = Math.floor(map.height / tileHeight);

    if (tile > tilesPerColum * tilesPerRow) {
      console.warn(`Tile ${tile} exceeds the map size`);
      return;
    }
    const tileX = ((tile - 1) % tilesPerRow) * tileWidth;
    const row = Math.floor((tile - 1) / tilesPerRow);
    const tileY = row * tileHeight;

    let worldLocation = camCorrection ? location.sub(this.game.camera.location): location;

    if (isometric) {
      worldLocation = this.game.helpers.converter.cartToIso(worldLocation);
    }
    if (alignment) {
      worldLocation = this.align(worldLocation, tileWidth, tileHeight, alignment);
    }
    if (manualCorrection) {
      worldLocation = worldLocation.add(manualCorrection);
    }

    ctx.drawImage(
      map, // image
      tileX, // source x
      tileY, // source y
      tileWidth, // source width
      tileHeight, // source height
      worldLocation.x,  // target x
      worldLocation.y, // target y
      scale.x, // target width
      scale.y // target height
    );

    if (debug) {
      ctx.strokeStyle = '#ff0000';
      ctx.beginPath();
      ctx.rect(worldLocation.x, worldLocation.y, scale.x, scale.y);
      ctx.stroke();
    }
  }

  private align(
    point: Vector,
    width: number,
    height: number,
    alignment: Alignment
  ): Vector {
    let deltaX, deltaY = 0;

    if (alignment.horizontal === 'center') {
      if (width < GRID_W) {
        deltaX = (GRID_W - width) / 2;
      } else if (width > GRID_W) {
        deltaX = -((width - GRID_W) / 2);
      }
    } else if (alignment.horizontal === 'end') {
      if (width < GRID_W) {
        deltaX = GRID_W - width;
      } else if (width > GRID_W) {
        deltaX = -(width - GRID_W);
      }
    }

    if (alignment.vertical === 'center') {
      if (height < GRID_H) {
        deltaY = (GRID_H - height) / 2;
      } else if (height > GRID_H) {
        deltaY = -((height - GRID_H) / 2);
      }
    } else if (alignment.vertical === 'bottom') {
      if (height < GRID_H) {
        deltaY = (GRID_H - height);
      } else if (height > GRID_H) {
        deltaY = -(height - GRID_H);
      }
    }

    return point.add(deltaX, deltaY);
  } 
}
