import { GRID_W } from 'game/settings';

import { Game, GameObject } from 'engine/core/game/base';
import { Vector } from 'engine/common/Vector';

import { Pathfinder } from './Pathfinder';
import { DrawTile, TileParams } from './DrawTile';
import { Converter } from './Converter';

export class GameHelpers extends GameObject {
  drawer: DrawTile;
  pathFinder: Pathfinder;
  converter: Converter;

  constructor(game: Game) {
    super(game);

    this.drawer = new DrawTile(game);
    this.pathFinder = new Pathfinder();
    this.converter = new Converter((this.game.ctx.canvas.width / 2) - GRID_W / 2, 0);
  }

  drawTile(params: TileParams): void {
    this.drawer.drawTile(params);
  }

  line(start: Vector, end: Vector, stroke?: string): void {
    if (stroke) {
      this.game.ctx.strokeStyle = stroke;
    }

    const isoStart = this.game.helpers.converter.cartToIso(
      this.game.camera.isoCorrection(start)
    );
    const isoEnd = this.game.helpers.converter.cartToIso(
      this.game.camera.isoCorrection(end)
    );

    this.game.ctx.beginPath();
    this.game.ctx.moveTo(isoStart.x, isoStart.y);
    this.game.ctx.lineTo(isoEnd.x, isoEnd.y);
    this.game.ctx.stroke();
  }

  shape(...points: Vector[]): void {
    const [start, ...rest] = points;
    const isoStart = this.game.helpers.converter.cartToIso(
      this.game.camera.isoCorrection(start)
    );

    this.game.ctx.beginPath();
    this.game.ctx.moveTo(isoStart.x, isoStart.y);
    rest.forEach(point => {
      const isoPoint = this.game.helpers.converter.cartToIso(
        this.game.camera.isoCorrection(point)
      );
      this.game.ctx.lineTo(isoPoint.x, isoPoint.y);
    });
    this.game.ctx.stroke();
    this.game.ctx.fill();
  }

  rect(start: Vector, width: number, height: number, stroke?: string): void{
    if (stroke) {
      this.game.ctx.fillStyle = stroke;
    }

    const startCam = this.game.helpers.converter.cartToIso(
      this.game.camera.isoCorrection(start)
    );

    this.game.ctx.translate(
      startCam.x,
      startCam.y,
    );

    this.game.ctx.transform(1, 0.5, -1, 0.5, 0, 0);

    this.game.ctx.beginPath();
    this.game.ctx.rect(0, 0, width, height);
    this.game.ctx.fill();

    this.game.ctx.resetTransform();
  }

  print(text: string, x: number, y: number): void {
    this.game.ctx.fillStyle = 'white';
    this.game.ctx.fillText(text, x, y);
    this.game.ctx.fillStyle = 'black';
  }
}
