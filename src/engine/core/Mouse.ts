import { throttle } from 'lodash';

import { Vector } from 'engine/common';
import EE from 'eventemitter3';

type Event = keyof HTMLElementEventMap;

export class Mouse {
  private ctx: CanvasRenderingContext2D;
  private emitter: EE;
  private registered: Event[];
  public location: Vector;

  constructor(context: CanvasRenderingContext2D) {
    this.ctx = context;
    this.location = new Vector(0, 0);
    this.trackMousePosition();
    this.emitter = new EE();
    this.registered = [];
  }

  public hookEvent(event: Event, callback: () => void, once = false): void {
    if (once) {
      this.emitter.once(event, callback);
    } else {
      this.emitter.on(event, callback);
    }
    if (!this.registered.includes(event)) {
      this.ctx.canvas.addEventListener(event, () => this.emitter.emit(event), false);
      this.registered.push(event);
    }
  }

  public removeEvent(event: Event, callback?: () => void, once = false): void {
    this.emitter.removeListener(event, callback, undefined, once);
  }

  private trackMousePosition() {
    this.ctx.canvas.addEventListener(
      'mousemove',
      throttle(this.setMouse.bind(this), 30),
      false
    );
  }

  private setMouse(e: MouseEvent) {
    this.location = this.getMousePos(e);
  }

  private getMousePos(evt: MouseEvent): Vector {
    const rect = this.ctx.canvas.getBoundingClientRect();
    return new Vector(
      Math.round(evt.clientX - rect.left),
      Math.round(evt.clientY - rect.top)
    );
  }
}
