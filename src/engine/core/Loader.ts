export class Loader {
  private images: Record<string, HTMLImageElement>;

  constructor() {
    this.images = {};
  }

  loadImage(key: string, src: string): Promise<HTMLImageElement> {
    const img = new Image();

    const sprite = new Promise<HTMLImageElement>((resolve, reject) => {
      img.onload = () => {
        this.images[key] = img;
        resolve(img);
      };

      img.onerror = () => {
        reject('Could not load image: ' + src);
      };
    });

    img.src = src;
    return sprite;
  }

  getImage(key: string): HTMLImageElement {
    return (key in this.images) ? this.images[key] : null;
  }
}
