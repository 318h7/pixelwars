import { Key } from 'engine/types';

export class Keyboard {
  private keys: Partial<Record<Key, boolean>>;
  private releaseCallbacks: Partial<Record<Key, () => void>>;
  private downCallbacks: Partial<Record<Key, () => void>>;

  constructor() {
    this.keys = {};
    this.releaseCallbacks = {};
    this.downCallbacks = {};

    window.addEventListener('keydown', this.onKeyDown.bind(this));
    window.addEventListener('keyup', this.onKeyUp.bind(this));
  }

  public registerKeys(keys: Key[]): void {
    this.keys = keys.reduce((map, key) => ({ ...map, [key]: false }), {});
  }

  public hookEvent(key: Key, downCb?: () => void, upCb?: () => void): void {
    if (downCb) {
      this.downCallbacks[key] = downCb;
    }
    if (upCb) {
      this.releaseCallbacks[key] = upCb;
    }
  }

  public isDown(keyCode: Key): boolean {
    return this.keys[keyCode as Key] ?? false;
  }

  private onKeyDown({ code }: KeyboardEvent): void {
    if (code in this.keys) {
      event.preventDefault();
      this.keys[code as Key] = true;
    }
    const callback = this.downCallbacks[code as Key];
    if (callback) {
      callback();
    }
  }

  private onKeyUp({ code }: KeyboardEvent): void {
    if (code in this.keys) {
      event.preventDefault();
      this.keys[code as Key] = false;
    }
    const callback = this.releaseCallbacks[code as Key];
    if (callback) {
      callback();
    }
  }
}
