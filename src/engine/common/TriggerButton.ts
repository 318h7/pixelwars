export class TriggerButton {
  callback: () => void;

  constructor(callback: () => void) {
    this.callback = callback;
  }

  trigger(): void {
    this.callback();
  }
}
