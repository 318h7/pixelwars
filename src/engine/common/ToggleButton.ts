import { RenderedObject } from '../core/game/base/GameObject';
import { Game } from '../core/game/base/Game';
import { Vector } from './Vector';

type ButtonStates = 'on' | 'onHover' | 'onActive' | 'off' | 'offHover' | 'offActive';

const defaultButtonFrames: Record<ButtonStates, number> = {
  on: 1,
  onHover: 2,
  onActive: 3,
  off: 4,
  offHover: 5,
  offActive: 6
};

export class ToggleButton extends RenderedObject {
  private STROKE: number;
  private dimensions: Vector;
  private color: string;
  private sprite: HTMLImageElement;
  private frames: Record<ButtonStates, number>;
  private frame: number;
  private updateValue: (state: boolean) => boolean;
  isOn: boolean;
  callback: (state: boolean) => void;

  constructor(
    game: Game,
    position: Vector,
    callback: (state: boolean) => void,
    sprite: HTMLImageElement,
    initialState?: boolean,
    updateValue?: (state: boolean) => boolean,
    frames = defaultButtonFrames,
  ) {
    super(game);
    this.position = position;
    this.isOn = initialState ?? false;
    this.callback = callback;
    this.dimensions = new Vector(16, 16);
    this.game.mouse.hookEvent('mouseup', () => this.toggle());
    this.game.mouse.hookEvent('mousedown', () => {
      this.mousedown();
    });
    this.updateValue = updateValue;
    this.sprite = sprite;
    this.frames = frames;
    this.frame = 1;
  }

  render(): void {
    this.drawButton();
    this.game.helpers.print(this.frame.toString(), 10, 10);
  }

  update(): void {
    if (this.updateValue) {
      this.isOn = this.updateValue(this.isOn) ?? this.isOn;
    }

    if (!this.isActive() && this.isMouseOver()) {
      this.frame = this.isOn ? this.frames.onHover : this.frames.offHover;
    } else {
      this.frame = this.isOn ? this.frames.on : this.frames.off;
    }
  }

  private isMouseOver(): boolean {
    return this.game.mouse.location.inside(this.position, this.position.add(this.dimensions));
  }

  private isActive(): boolean {
    return this.frame === this.frames.onActive || this.frame === this.frames.offActive;
  }

  private drawButton() {
    this.game.helpers.drawTile({
      map: this.sprite,
      tile: this.frame,
      location: this.position,
      tileWidth: this.dimensions.x,
      tileHeight: this.dimensions.y,
      isometric: false,
      camCorrection: false,
    });
  }

  toggle(): void {
    if (this.isMouseOver()) {
      this.isOn = !this.isOn;
      this.callback(this.isOn);
    }
  }

  mousedown(): void {
    if (this.isMouseOver()) {
      this.frame = this.isOn ? this.frames.onActive : this.frames.offActive;
    }
  }
}
