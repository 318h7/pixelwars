import { clamp } from 'lodash';

/*
Immutable Vector math implementation
*/

export class Vector {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  public reset(): void {
    this.x = 0;
    this.y = 0;
  }

  public isEqual(point: Vector | undefined): boolean {
    if (!point) return false;
    return point.x === this.x && point.y === this.y;
  }

  public isClose({ x, y }: Vector, distance = 3): boolean {
    return Math.abs(x - this.x) < distance && Math.abs(y - this.y) < distance;
  }

  public isZero(): boolean {
    return this.x === 0 && this.y === 0;
  }

  public negate(): Vector {
    return new Vector(-this.x, -this.y);
  }

  public map(min: number, max: number, newMin: number, newMax: number): Vector {
    return new Vector(
      (this.x - min) * (newMax - newMin) / (max - min) + newMin,
      (this.y - min) * (newMax - newMin) / (max - min) + newMin,
    );
  }

  inside(start: Vector, end: Vector): boolean {
    return this.x >= start.x && this.x <= end.x &&
      this.y >= start.y && this.y <= end.y;
  }

  dist(point: Vector): number {
    return point
      .sub(this)
      .mag();
  }

  mag(): number {
    return Math.sqrt(this.magSq());
  }

  setMag(magnitude: number): Vector {
    return this.normalize().mult(magnitude);
  }

  magSq(): number {
    return this.x * this.x + this.y * this.y;
  }

  clamp(min: number, max: number): Vector {
    return new Vector(
      clamp(this.x, min, max),
      clamp(this.y, min, max),
    );
  }

  normalize(): Vector {
    const len = this.mag();
    if (len !== 0) {
      return this.mult(1 / len);
    }
    return new Vector(this.x, this.y);
  }

  limit(max: number): Vector {
    const mSq = this.magSq();
    if (mSq > max * max) {
      return this.div(Math.sqrt(mSq)) //normalize it
        .mult(max);
    }
    return new Vector(this.x, this.y);
  }

  lerp({ x, y }: Vector, amt: number): Vector {
    return new Vector(
      this.x + (((x - this.x) * amt) || 0),
      this.y + (((y - this.y) * amt) || 0),
    );
  }

  public round(): Vector {
    return new Vector(Math.round(this.x), Math.round(this.y));
  }

  public sub(point: Vector): Vector {
    return new Vector(this.x - point.x, this.y - point.y);
  }

  public add(...points: Array<Vector | number>): Vector {
    if (points[0] instanceof Vector) {
      const { x, y } = points[0] as Vector;
      return new Vector(this.x + x, this.y + y);
    }
    if (points.length > 1) {
      const [a, b] = points as number[];
      return new Vector(this.x + a, this.y + b);
    }
  }

  public mult(mult: number): Vector {
    return new Vector(this.x * mult, this.y * mult);
  }

  public div(div: number): Vector {
    return new Vector(this.x / div, this.y / div);
  }

  public moveX(delta: number): Vector {
    return new Vector(this.x + delta, this.y);
  }

  public moveY(delta: number): Vector {
    return new Vector(this.x, this.y + delta);
  }

  public setX(newX: number): Vector {
    return new Vector(newX, this.y);
  }

  public setY(newY: number): Vector {
    return new Vector(this.x, newY);
  }

  public toString(): string {
    return `x: ${this.x} y: ${this.y}`;
  }
}
