import { Battle } from 'game/scenes/Battle';
import { Game, Hero } from 'engine/core';
import { Vector } from 'engine/common';
import { skeletonParams } from 'game/settings';

import 'styles/styles.css';
import { orbAnimations, orbSetting } from 'game/settings/orb';

/*
Main entrypoint
*/
window.onload = function () {
  const game = new Game({ width: 640, height: 360, rootId: 'game' });

  const load = [
    game.loader.loadImage('orb', '../assets/plasma ball.png'),
    game.loader.loadImage('skeleton', '../assets/isoDepth/skeleton8.png'),
  ];

  Promise.all(load).then(() => {
    game.addScene('battle', Battle);
    const skel = new Hero(
      game,
      game.loader.getImage('skeleton'),
      skeletonParams,
      new Vector(2, 2),
      new Vector(0, 16),
    );
    skel.addAbility(orbSetting, orbAnimations);

    game.addHero('skeleton', skel, true);
    game.renderScene('battle');
  });
};
