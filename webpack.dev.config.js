const path = require('path')
const { merge } = require('webpack-merge');

const base = require('./webpack.base');

module.exports = merge(base, {
  mode: 'development',
  devtool: 'cheap-source-map',
  devServer: {
    https: false,
    disableHostCheck: true,
    contentBase: path.resolve(__dirname, 'dist'),
    compress: true,
    port: 3000,
    watchOptions: {
      aggregateTimeout: 500,
      ignored: ['**/vendor.js', 'node_modules/**'],
    },
  },
});
