module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: [
    '@typescript-eslint',
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  "env": {
    "browser": true,
    "jest": true,
  },
  rules: {
    "yoda": ["error", "never"],
    "camelcase": "error",
    "eol-last": ["error"],
    "indent": ["error", 2],
    "quotes": ["error", "single"],
    "max-len": ["error", { code: 100, ignoreComments: true, ignoreUrls: true }],
    "no-unused-vars": 'off',
    "@typescript-eslint/no-unused-vars": ['error', { "argsIgnorePattern": "^_"  }],
    "semi": ["error", "always"],
  },
};
