const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
  WEBGL_RENDERER: true,
  CANVAS_RENDERER: true
})

module.exports = {
  mode: 'none',
  entry: {
    main: './src/main.ts',
  },
  output: {
    filename: '[name].js',
  },
  plugins: [
    definePlugin,
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './static/index.html',
      hash: false
    }),
    new CopyPlugin({
      patterns: [
        { from: "./assets", to: "assets" },
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: require.resolve('ts-loader'),
        include: path.join(__dirname, 'src'),
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    plugins: [new TsconfigPathsPlugin()],
  }
}
